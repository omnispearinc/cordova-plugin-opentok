package com.tokbox.cordova;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Camera;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.Display;
import android.view.WindowManager;
import com.opentok.android.BaseVideoCapturer;
import com.opentok.android.Publisher.CameraCaptureFrameRate;
import com.opentok.android.Publisher.CameraCaptureResolution;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

@TargetApi(21)
public class AutofocusVideoCapturer extends BaseVideoCapturer implements BaseVideoCapturer.CaptureSwitch {
    private static final String TAG = "AutofocusVideoCapturer";
    private static final int IMAGE_FORMAT = ImageFormat.YUV_420_888;
    private CameraManager cameraManager;
    private CameraDevice camera;
    private HandlerThread camThread;
    private Handler camHandler;
    private ImageReader cameraFrame;
    private CaptureRequest captureRequest;
    private CaptureRequest.Builder captureRequestBuilder;
    private CameraCaptureSession captureSession;
    private CameraInfoCache characteristics;
    private CameraState cameraState;
    private Display display;
    private DisplayOrientationCache displayOrientationCache;
    private ReentrantLock reentrantLock;
    private Condition condition;
    private int cameraIndex;
    private Size frameDimensions;
    private int desiredFps;
    private Range<Integer> camFps;
    private List<RuntimeException> runtimeExceptionList;
    private boolean isPaused;
    private static final SparseIntArray rotationTable = new SparseIntArray() {
        {
            this.append(0, 0);
            this.append(1, 90);
            this.append(2, 180);
            this.append(3, 270);
        }
    };
    private static final SparseArray<Size> resolutionTable = new SparseArray<Size>() {
        {
            this.append(CameraCaptureResolution.LOW.ordinal(), new Size(352, 288));
            this.append(CameraCaptureResolution.MEDIUM.ordinal(), new Size(640, 480));
            this.append(CameraCaptureResolution.HIGH.ordinal(), new Size(1280, 720));
        }
    };
    private static final SparseIntArray frameRateTable = new SparseIntArray() {
        {
            this.append(CameraCaptureFrameRate.FPS_1.ordinal(), 1);
            this.append(CameraCaptureFrameRate.FPS_7.ordinal(), 7);
            this.append(CameraCaptureFrameRate.FPS_15.ordinal(), 15);
            this.append(CameraCaptureFrameRate.FPS_30.ordinal(), 30);
        }
    };
    private final CameraDevice.StateCallback cameraObserver = new CameraDevice.StateCallback() {
        public void onOpened(@NonNull CameraDevice camera) {
            Log.d(TAG, "CameraState.onOpened");
            AutofocusVideoCapturer.this.cameraState = CameraState.OPEN;
            AutofocusVideoCapturer.this.camera = camera;
            AutofocusVideoCapturer.this.signalCamStateChange();
        }

        public void onDisconnected(@NonNull CameraDevice camera) {
            Log.d(TAG, "CameraState.onDisconnected");
            AutofocusVideoCapturer.this.camera.close();
            AutofocusVideoCapturer.this.waitForCamStateChange(AutofocusVideoCapturer.this.cameraState);

        }

        public void onError(@NonNull CameraDevice camera, int error) {
            Log.d(TAG, "CameraState.onError");
            AutofocusVideoCapturer.this.camera.close();
            AutofocusVideoCapturer.this.waitForCamStateChange(AutofocusVideoCapturer.this.cameraState);
            AutofocusVideoCapturer.this.postAsyncException(new AutofocusException("Camera Open Error: " + error));
        }

        public void onClosed(@NonNull CameraDevice camera) {
            super.onClosed(camera);
            Log.d(TAG, "CameraState.onClosed");
            AutofocusVideoCapturer.this.cameraState = CameraState.CLOSED;
            AutofocusVideoCapturer.this.camera = null;
            AutofocusVideoCapturer.this.signalCamStateChange();
        }
    };
    private ImageReader.OnImageAvailableListener frameObserver = new ImageReader.OnImageAvailableListener() {
        public void onImageAvailable(ImageReader reader) {
            Image frame = reader.acquireNextImage();
            if (AutofocusVideoCapturer.this.cameraState == CameraState.CAPTURE) {
                AutofocusVideoCapturer.this.provideBufferFramePlanar(frame.getPlanes()[0].getBuffer(), frame.getPlanes()[1].getBuffer(), frame.getPlanes()[2].getBuffer(), frame.getPlanes()[0].getPixelStride(), frame.getPlanes()[0].getRowStride(), frame.getPlanes()[1].getPixelStride(), frame.getPlanes()[1].getRowStride(), frame.getPlanes()[2].getPixelStride(), frame.getPlanes()[2].getRowStride(), frame.getWidth(), frame.getHeight(), AutofocusVideoCapturer.this.calculateCamRotation(), AutofocusVideoCapturer.this.isFrontCamera());
            }
            frame.close();
        }
    };
    private CameraCaptureSession.StateCallback captureSessionObserver = new CameraCaptureSession.StateCallback() {
        public void onConfigured(@NonNull CameraCaptureSession session) {
            Log.d(TAG, "CaptureState.onConfigured");
            try {
                AutofocusVideoCapturer.this.cameraState = CameraState.CAPTURE;
                AutofocusVideoCapturer.this.captureSession = session;
                AutofocusVideoCapturer.this.captureRequest = AutofocusVideoCapturer.this.captureRequestBuilder.build();
                AutofocusVideoCapturer.this.captureSession.setRepeatingRequest(AutofocusVideoCapturer.this.captureRequest, AutofocusVideoCapturer.this.captureNotification, AutofocusVideoCapturer.this.camHandler);
                AutofocusVideoCapturer.this.signalCamStateChange();
            } catch (CameraAccessException cae) {
                cae.printStackTrace();
            }

        }

        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
            Log.d(TAG, "CaptureState.onConfigureFailed");
            AutofocusVideoCapturer.this.cameraState = CameraState.ERROR;
            AutofocusVideoCapturer.this.postAsyncException(new AutofocusException("Camera session configuration failed"));
            AutofocusVideoCapturer.this.signalCamStateChange();
        }

        public void onClosed(@NonNull CameraCaptureSession session) {
            Log.d(TAG, "CaptureState.onClosed");
            AutofocusVideoCapturer.this.camera.close();
        }
    };
    private CameraCaptureSession.CaptureCallback captureNotification = new CameraCaptureSession.CaptureCallback() {
        public void onCaptureStarted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, long timestamp, long frameNumber) {
            super.onCaptureStarted(session, request, timestamp, frameNumber);
        }
    };

    public AutofocusVideoCapturer(Context ctx, CameraCaptureResolution resolution, CameraCaptureFrameRate fps, boolean isFront) throws AutofocusException {
        this.cameraManager = (CameraManager)ctx.getSystemService(Context.CAMERA_SERVICE);
        this.display = ((WindowManager) Objects.requireNonNull(ctx.getSystemService(Context.WINDOW_SERVICE))).getDefaultDisplay();
        this.camera = null;
        this.cameraState = CameraState.CLOSED;
        this.reentrantLock = new ReentrantLock();
        this.condition = this.reentrantLock.newCondition();
        this.frameDimensions = resolutionTable.get(resolution.ordinal());
        this.desiredFps = frameRateTable.get(fps.ordinal());
        this.runtimeExceptionList = new ArrayList();
        this.isPaused = false;

        try {
            Log.i(TAG, (isFront ? "Front " : "Back ") + "Camera requested.");
            int lensDirection = isFront ? CameraCharacteristics.LENS_FACING_FRONT : CameraCharacteristics.LENS_FACING_BACK;
            String camId = this.selectCamera(lensDirection);
            this.cameraIndex = this.findCameraIndex(camId);
        } catch (CameraAccessException cae) {
            throw new AutofocusException(cae.getMessage());
        }
    }

    public synchronized void init() {
        Log.d(TAG,"init entered");
        this.characteristics = null;
        this.startCamThread();
        this.startDisplayOrientationCache();
        this.initCamera();
        Log.d(TAG,"init complete.");
    }

    public synchronized int startCapture() {
        Log.d(TAG,"OT.startCapture");
        if (this.camera != null && this.cameraState == CameraState.OPEN) {
            try {
                CameraCharacteristics characteristics = this.cameraManager.getCameraCharacteristics(this.camera.getId());

                this.captureRequestBuilder = this.camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                this.captureRequestBuilder.addTarget(this.cameraFrame.getSurface());
                this.captureRequestBuilder.set(CaptureRequest.CONTROL_AE_TARGET_FPS_RANGE, this.camFps);
                this.captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CaptureRequest.CONTROL_MODE_AUTO);

                int[] afModes = characteristics.get(CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES);
                Arrays.sort(afModes);
                if (Arrays.binarySearch(afModes, CameraCharacteristics.CONTROL_AF_MODE_CONTINUOUS_VIDEO) > -1) {
                    this.captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_VIDEO);
                } else if (Arrays.binarySearch(afModes, CameraCharacteristics.CONTROL_AF_MODE_AUTO) > -1) {
                    this.captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO);
                    this.captureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CaptureRequest.CONTROL_AF_TRIGGER_START);
                } else {
                    this.captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_OFF);
                }

                this.camera.createCaptureSession(Arrays.asList(this.cameraFrame.getSurface()), this.captureSessionObserver, this.camHandler);

                this.waitForCamStateChange(CameraState.OPEN);
                return 0;
            } catch (CameraAccessException cae) {
                throw new AutofocusException(cae.getMessage());
            }
        } else {
            throw new AutofocusException("Start Capture called before init successfully completed.");
        }
    }

    public synchronized int stopCapture() {
        Log.d(TAG,"OT.stopCapture");
        if (this.camera != null && this.captureSession != null && this.cameraState != CameraState.CLOSED) {
            CameraState oldState = this.cameraState;
            this.captureSession.close();
            this.waitForCamStateChange(oldState);
            this.cameraFrame.close();
            this.characteristics = null;
        }
        return 0;
    }

    public synchronized void destroy() {
        this.stopDisplayOrientationCache();
        this.stopCamThread();
    }

    public boolean isCaptureStarted() {
        return this.cameraState == CameraState.CAPTURE;
    }

    public synchronized CaptureSettings getCaptureSettings() {
        CaptureSettings retObj = new CaptureSettings();
        retObj.fps = this.desiredFps;
        retObj.width = this.cameraFrame != null ? this.cameraFrame.getWidth() : 0;
        retObj.height = this.cameraFrame != null ? this.cameraFrame.getHeight() : 0;
        retObj.format = 1;
        retObj.expectedDelay = 0;
        return retObj;
    }

    public synchronized void onPause() {
        Log.d(TAG,"OT.onPause");
        switch(this.cameraState) {
            case CAPTURE:
                this.stopCapture();
                this.isPaused = true;
            case SETUP:
            default:
        }
    }

    public void onResume() {
        Log.d(TAG,"OT.onResume");
        if (this.isPaused) {
            this.initCamera();
            this.startCapture();
            this.isPaused = false;
        }

    }

    public synchronized void cycleCamera() {
        try {
            String[] cameraIds = this.cameraManager.getCameraIdList();
            this.swapCamera((this.cameraIndex + 1) % cameraIds.length);
        } catch (CameraAccessException cae) {
            cae.printStackTrace();
            throw new AutofocusException(cae.getMessage());
        }
    }

    public int getCameraIndex() {
        return this.cameraIndex;
    }

    public synchronized void swapCamera(int cameraIndex) {
        Log.i(TAG, "Swap camera to " + cameraIndex);
        CameraState oldState = this.cameraState;
        switch(oldState) {
            case CAPTURE:
                this.stopCapture();
            case SETUP:
            default:
                this.cameraIndex = cameraIndex;
                switch(oldState) {
                    case CAPTURE:
                        this.initCamera();
                        this.startCapture();
                    case SETUP:
                    default:
                }
        }
    }

    private boolean isFrontCamera() {
        return this.characteristics != null && this.characteristics.isFrontFacing();
    }

    private void waitForCamStateChange(CameraState oldState) throws RuntimeException {
        this.reentrantLock.lock();

        try {
            Log.d(TAG,"wait for change from " + oldState.name());

            while(this.cameraState == oldState) {
                this.condition.await();
            }
        } catch (InterruptedException ie) {
            this.waitForCamStateChange(oldState);
        }

        this.reentrantLock.unlock();
        Iterator exceptionIterator = this.runtimeExceptionList.iterator();
        if (exceptionIterator.hasNext()) {
            throw (RuntimeException)exceptionIterator.next();
        } else {
            this.runtimeExceptionList.clear();
        }
    }

    private void signalCamStateChange() {
        this.reentrantLock.lock();
        this.condition.signalAll();
        this.reentrantLock.unlock();
    }

    private void startCamThread() {
        this.camThread = new HandlerThread("Camera-Thread");
        this.camThread.start();
        this.camHandler = new Handler(this.camThread.getLooper());
    }

    private void stopCamThread() {
        try {
            this.camThread.quitSafely();
            this.camThread.join();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        } finally {
            this.camThread = null;
            this.camHandler = null;
        }

    }

    // front = 0, back = 1, external = 2
    private String selectCamera(int lensDirection) throws CameraAccessException {
        String[] cameraIds = this.cameraManager.getCameraIdList();
        for (String id : cameraIds) {
            CameraCharacteristics info = this.cameraManager.getCameraCharacteristics(id);
            if (info.get(CameraCharacteristics.LENS_FACING) == lensDirection) {
                Log.i(TAG, (lensDirection == 0 ? "Front " : "Back ") + "Camera selected. ID: " + id);
                return id;
            }
        }
        return null;
    }

    private Range<Integer> selectCameraFpsRange(String camId, final int fps) throws CameraAccessException {
        String[] cameraIds = this.cameraManager.getCameraIdList();

        for(String id : cameraIds) {
            if (id.equals(camId)) {
                CameraCharacteristics info = this.cameraManager.getCameraCharacteristics(id);
                List<Range<Integer>> fpsLst = new ArrayList();
                Collections.addAll(fpsLst, info.get(CameraCharacteristics.CONTROL_AE_AVAILABLE_TARGET_FPS_RANGES));
                return Collections.min(fpsLst, new Comparator<Range<Integer>>() {
                    private static final int MAX_FPS_DIFF_THRESHOLD = 5000;
                    private static final int MAX_FPS_LOW_DIFF_WEIGHT = 1;
                    private static final int MAX_FPS_HIGH_DIFF_WEIGHT = 3;
                    private static final int MIN_FPS_THRESHOLD = 8000;
                    private static final int MIN_FPS_LOW_VALUE_WEIGHT = 1;
                    private static final int MIN_FPS_HIGH_VALUE_WEIGHT = 4;

                    private int progressivePenalty(int value, int threshold, int lowWeight, int highWeight) {
                        return value < threshold ? value * lowWeight : threshold * lowWeight + (value - threshold) * highWeight;
                    }

                    private int diff(Range<Integer> val) {
                        int minFpsError = this.progressivePenalty(val.getLower(), MIN_FPS_THRESHOLD, MIN_FPS_LOW_VALUE_WEIGHT, MIN_FPS_HIGH_VALUE_WEIGHT);
                        int maxFpsError = this.progressivePenalty(Math.abs(fps * 1000 - val.getUpper()), MAX_FPS_DIFF_THRESHOLD, MAX_FPS_LOW_DIFF_WEIGHT, MAX_FPS_HIGH_DIFF_WEIGHT);
                        return minFpsError + maxFpsError;
                    }

                    public int compare(Range<Integer> lhs, Range<Integer> rhs) {
                        return this.diff(lhs) - this.diff(rhs);
                    }
                });
            }
        }

        return null;
    }

    private int findCameraIndex(String camId) throws CameraAccessException {
        String[] cameraIds = this.cameraManager.getCameraIdList();

        for(int i = 0; i < cameraIds.length; i++) {
            if (cameraIds[i].equals(camId)) {
                return i;
            }
        }

        return -1;
    }

    private Size selectPreferredSize(String camId, final int width, final int height, int format) throws CameraAccessException {
        CameraCharacteristics info = this.cameraManager.getCameraCharacteristics(camId);
        StreamConfigurationMap dimMap = info.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        List<Size> sizeLst = new ArrayList();
        Collections.addAll(sizeLst, dimMap.getOutputSizes(IMAGE_FORMAT));
        return Collections.min(sizeLst, new Comparator<Size>() {
            public int compare(Size lhs, Size rhs) {
                int lXerror = Math.abs(lhs.getWidth() - width);
                int lYerror = Math.abs(lhs.getHeight() - height);
                int rXerror = Math.abs(rhs.getWidth() - width);
                int rYerror = Math.abs(rhs.getHeight() - height);
                return lXerror + lYerror - (rXerror + rYerror);
            }
        });
    }

    private int calculateCamRotation() {
        if (this.characteristics != null) {
            int cameraRotation = this.displayOrientationCache.getOrientation();
            int cameraOrientation = this.characteristics.sensorOrientation();
            return !this.characteristics.isFrontFacing() ? Math.abs((cameraRotation - cameraOrientation) % 360) : (cameraRotation + cameraOrientation + 360) % 360;
        } else {
            return 0;
        }
    }

    @SuppressLint({"all"})
    private void initCamera() {
        try {
            this.cameraState = CameraState.SETUP;
            String[] cameraIdList = this.cameraManager.getCameraIdList();
            String camId = cameraIdList[this.cameraIndex];
            this.camFps = this.selectCameraFpsRange(camId, this.desiredFps);
            Size preferredSize = this.selectPreferredSize(camId, this.frameDimensions.getWidth(), this.frameDimensions.getHeight(), IMAGE_FORMAT);
            this.cameraFrame = ImageReader.newInstance(preferredSize.getWidth(), preferredSize.getHeight(), IMAGE_FORMAT, 3);
            this.cameraFrame.setOnImageAvailableListener(this.frameObserver, this.camHandler);
            this.characteristics = new CameraInfoCache(this.cameraManager.getCameraCharacteristics(camId));
            this.cameraManager.openCamera(camId, this.cameraObserver, this.camHandler);
            this.waitForCamStateChange(CameraState.SETUP);
        } catch (CameraAccessException cae) {
            throw new AutofocusException(cae.getMessage());
        }
    }

    private void postAsyncException(RuntimeException exp) {
        this.runtimeExceptionList.add(exp);
    }

    private void startDisplayOrientationCache() {
        this.displayOrientationCache = new DisplayOrientationCache(this.display, this.camHandler);
    }

    private void stopDisplayOrientationCache() {
        this.camHandler.removeCallbacks(this.displayOrientationCache);
    }

    public static class AutofocusException extends RuntimeException {
        public AutofocusException(String message) {
            super(message);
            Log.e(TAG, "AutofocusException: " + message);
        }
    }

    private static class DisplayOrientationCache implements Runnable {
        private static final long POLL_DELAY_MS = 750;
        private int displayRotation;
        private Display display;
        private Handler handler;

        public DisplayOrientationCache(Display dsp, Handler hndlr) {
            this.display = dsp;
            this.handler = hndlr;
            this.displayRotation = rotationTable.get(this.display.getRotation());
            this.handler.postDelayed(this, POLL_DELAY_MS);
        }

        public int getOrientation() {
            return this.displayRotation;
        }

        public void run() {
            this.displayRotation = rotationTable.get(this.display.getRotation());
            this.handler.postDelayed(this, POLL_DELAY_MS);
        }
    }

    private static class CameraInfoCache {
        private CameraCharacteristics info;
        private boolean frontFacing;
        private int sensorOrientation;

        public CameraInfoCache(CameraCharacteristics info) {
            this.info = info;
            this.frontFacing = info.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT;
            this.sensorOrientation = info.get(CameraCharacteristics.SENSOR_ORIENTATION);
        }

        public <T> T get(CameraCharacteristics.Key<T> key) {
            return this.info.get(key);
        }

        public boolean isFrontFacing() {
            return this.frontFacing;
        }

        public int sensorOrientation() {
            return this.sensorOrientation;
        }
    }

    private enum CameraState {
        CLOSED,
        SETUP,
        OPEN,
        CAPTURE,
        ERROR
    }
}
